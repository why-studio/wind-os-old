# WindOS

#### 介绍
初中生自制操作系统，根据《30天自制操作系统》源码修改而来。

#### 软件架构
1. WindOS00aa是源码，开发代号00aa
2. z_打头的是工具以及其他必备组件


#### 安装教程

1.  装载windos.img
2.  使用windos.img引导系统
3.  敬请享受吧~

#### 使用说明

1.  Shift+F1可以快捷关闭应用程序窗口
2.  Shift+F2可以再打开一个命令行窗口（本操作系统支持多任务哦！）
3.  呃，自己探索吧~

#### 计划更新列表

<!--
| 序号 | 内容 | 当前状态 |
|----|----|------|
| 1  | 中文字库   |   正在努力加入中（日文字库已加入）   |
| 2  |  从硬盘读取数据  |   正在探索方式   |
| 3  | 修复已知bug（winver组件）| 具体把系统字库搞崩掉的办法还在找 |
| 4  | 任何人类需要的内容 | -- |

P.S.如果你有成熟的解决方案，欢迎Pull Request！-->
<b><a color="#FF0000">注意！本项目已经停止维护，目前作者正在开发面向UEFI的新一代操作系统，旧的技术将会被抛弃，打造一个没有历史包袱的全新操作系统！</a></b>

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
